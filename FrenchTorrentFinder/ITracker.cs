﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrenchTorrentFinder.Enums;

namespace FrenchTorrentFinder
{
    public abstract class ITracker
    {
        public abstract  ResultItem GetTorrent(ResultItem rItm);

        public abstract Task<List<ResultItem>> SearchTorrent(string keywords, Categorie categorie);

        public async Task<List<ResultItem>> SearchTorrent(string[] keywords, Categorie categorie)
        {
            int i = 0;
            var result = new List<ResultItem>();
            while (i < keywords.Length && !result.Any())
            {
                var r = this.SearchTorrent(keywords[i], Categorie.Tv).Result;
                if (r.Any())
                {
                    result.AddRange(r);
                    break;
                }
                i++;
            }
            return result;
        }

}
}
