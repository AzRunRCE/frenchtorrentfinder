﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrenchTorrentFinder
{
    public static class CleanKeyword
    {
        public static string Clean(string keyword)
        {
  
            keyword = keyword.Replace("'", " ");
            keyword = keyword.Replace("chapter-", "");
            keyword = keyword.Replace("ième-partie", "");
            keyword = keyword.Replace("la-", "");
            keyword = keyword.Replace("épisode", "");
            keyword = keyword.Replace("de-", "");
            keyword = keyword.Replace("-", " ");
            keyword = keyword.Replace(":", " ");
            if (keyword.EndsWith(" "))
                keyword = keyword.Remove(keyword.Length - 1);
            return keyword;
        }

    }
}
