﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace FrenchTorrentFinder
{
    public  class Context
    {
        public static string PythonPath { get; set; }
        public Context(IConfiguration configuration)
        {
            PythonPath = configuration["PythonPath"];
        }

    }
}
