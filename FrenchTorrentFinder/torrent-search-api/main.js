const TorrentSearchApi = require('torrent-search-api');
var uniqueFilename = require('unique-filename')
 const os = require('os')
// Enable private provider with credentials
TorrentSearchApi.enableProvider('Yggtorrent', 'YGG_TORRENT_LOGIN', 'YGG_TORRENT_PASSWORD');
const kewword =  process.argv[2];
const categorie =  process.argv[3];
const path = require('path');
var fs = require('fs');

var dir = 'Torrents';
if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}

(async () => {
	
	if (kewword == "download"){
	var torrent = {}
	torrent.link = process.argv[4];
	torrent.provider = process.argv[3];
	TorrentSearchApi.search("1080", "Movie", 10)
	var rndFileName = uniqueFilename('') + '.torrent'
	var outPutPath =  path.join('Torrents',rndFileName) 
	await TorrentSearchApi.downloadTorrent(torrent,outPutPath )
	process.stdout.write(rndFileName)
}
else {
	TorrentSearchApi.search(kewword, categorie, 20).then(torrents => {
		torrents.forEach(function (element) {
		  element.time = null;
		});
		console.log(torrents);
	   });
	       
}
})();



