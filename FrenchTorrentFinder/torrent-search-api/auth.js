var request = require('request');
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0
const fs = require('fs');
var uniqueFilename = require('unique-filename')
var cloudscraper = require('cloudscraper').defaults({ 'proxy': 'http://127.0.0.1:8888', followAllRedirects: true });
var j = request.jar();
const path = require('path');
const torrentPage = process.argv[2];

getSession()
    .then(Authentificate)
    .then(getTorrent)
    .catch(console.error);



function getTorrent() {

    var rndFileName = uniqueFilename('') + '.torrent'
    var outPutPath = path.join('Torrents', rndFileName)

    /* Create an empty file where we can save data */
    let file = fs.createWriteStream(outPutPath);
    request({
        jar: j,
        /* Here you should specify the exact link to the file you are trying to download */
        uri: torrentPage,
        'headers': {
            'User-Agent': ' Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36',
            'Referer': ' https://www2.yggtorrent.se/',
            'Accept-Encoding': ' gzip, deflate, br',
            'Accept-Language': ' fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7'
        },
        /* GZIP true for most of the websites now, disable it if you don't need it */
        gzip: true
    })
        .pipe(file)
        .on('finish', () => {
            process.stdout.write(rndFileName)

        })
        .on('error', (error) => {

        });
}

function Authentificate() {
    var options = {
        uri: 'https://www2.yggtorrent.se/user/login',
        jar: j,
        'headers': {
            'User-Agent': ' Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36',
            'Referer': ' https://www2.yggtorrent.se/',
            'Accept-Encoding': ' gzip, deflate, br',
            'Accept-Language': ' fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7'
        },
        formData: {
            'id': '',
            'pass': '',
            'ci_csrf_token': ''
        },
    };
   return cloudscraper.post(options)
}

function getSession() {
  return cloudscraper.get({
          method: 'GET',
          'headers': {
              'User-Agent': ' Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36',
              'Referer': ' https://www2.yggtorrent.se/',
              'Accept-Encoding': ' gzip, deflate, br',
              'Accept-Language': ' fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7'
          },
        jar: j, // Custom cookie jar
        url: 'https://www2.yggtorrent.se',
    })
}



