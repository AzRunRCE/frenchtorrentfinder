﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrenchTorrentFinder
{
    public class FormatResult
    {
        public string Format(ResultItem itm)
        {
            var str = $"<!DOCTYPE html><html><body><a href=\"{itm.Value}\"></a></body></html>";
            return str;
        }

        public string Format(List<ResultItem> itms)
        {
            var str = "<!DOCTYPE html>\n<head>\n<title></title>\n<meta charset=\"UTF-8\" />\n<body>\n<tbody>";
            for (int i = 0; i < itms.Count; i++)
            {
                ResultItem elm = itms[i];
                str += "<tr></tr>\n<tr><td>\n<a href=\"https://azrunsoft.ddns.net/api/Torrent/" + i + "\">" + elm.Title + "</a></td>\n";
                str += "<td>" + elm.Size + "</td>\n";
                str += "<td>" + elm.Seeds + "</td>\n";
                str += "<td>" + elm.Peers + "</td>\n</tr>\n";
            }
            return str;
        }
    }
}
