﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrenchTorrentFinder.Trackers;

namespace FrenchTorrentFinder
{
    public static class TorrentContainer
    {
        private static List<ResultItem> Torrents { get; set; } = new List<ResultItem>();

        public static List<ResultItem> Set(List<ResultItem> resultItems)
        {
            Torrents.Clear();
            if (resultItems.Any())
                Torrents.AddRange(resultItems);
            return Torrents;
        }

        internal static ResultItem GetResultItem(int id)
        {
            if (id < Torrents.Count && Torrents[id] != null)
            {
                ITracker tracker = (ITracker)Activator.CreateInstance(Type.GetType("FrenchTorrentFinder.Trackers." + Torrents[id].provider));
                return tracker.GetTorrent(Torrents[id]);
            }
            else
                throw new Exception("Aucun torrent pour cet Id");
        }

        internal static void Clear()
        {
            Torrents.Clear();
        }
    }
}
