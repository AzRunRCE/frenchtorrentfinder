import cfscrape
import sys
if __name__ == "__main__":
    from sys import argv
    scraper = cfscrape.create_scraper()
    if len(argv) == 2:
        sys.stdout.buffer.write(scraper.get(argv[1]).content)
    else:
        print("input error")
    exit()
