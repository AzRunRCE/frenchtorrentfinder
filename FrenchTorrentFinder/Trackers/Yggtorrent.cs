﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FrenchTorrentFinder.Enums;
using Newtonsoft.Json;

namespace FrenchTorrentFinder.Trackers
{
    public class Yggtorrent : ITracker
    {
        public override ResultItem GetTorrent(ResultItem resultItem)
        {
            var proc = new CallExternalProgram();
            var fileName = proc.RunPyhonFile(proc.GetPackagePath("node"), Path.Combine(Directory.GetCurrentDirectory(), "torrent-search-api", "auth.js"), new string[] { resultItem.link });
            resultItem.Value = "https://azrunsoft.ddns.net/api/Torrent/Download/" + fileName;
            //resultItem.Value = "http://192.168.0.29:1337/api/Torrent/Download/" + fileName;
            return resultItem;
        }

        public override async Task<List<ResultItem>> SearchTorrent(string keyword, Categorie categorie)
        {
            try
            {
                keyword = CleanKeyword.Clean(keyword); 

                Console.WriteLine($"{this.GetType().Name} {keyword} {categorie}");
                var proc = new CallExternalProgram();
                var html = proc.RunPyhonFile(proc.GetPackagePath("node"), Path.Combine(Directory.GetCurrentDirectory(),"torrent-search-api" , "main.js"), new string[] { keyword, categorie.ToString() });
                var output =  JsonConvert.DeserializeObject<List<ResultItem>>(html);
                output.ForEach(x => x.Categorie = categorie.ToString());
                return output;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<ResultItem>();

            }
        }
    }
}

