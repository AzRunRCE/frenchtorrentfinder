﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.Extensions.Configuration;

namespace FrenchTorrentFinder
{
    public class Torrent9V1 : Trackers.Torrent9
    {
        public new static string HOST { get { return "https://wvwv.agence-webside.fr/"; } }
        public override string SEARCH_PATH { get { return "search_torrent/"; } }
        public virtual string KEYWORD_SUFFIX { get { return ".html"; } }

        public override string XPATH_SEED { get { return "/html/body/div[1]/div/div/div/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/div/ul[1]/li[3]"; } }
        public override string XPATH_LEECH { get { return "/html/body/div[1]/div/div/div/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/div/ul[1]/li[7]"; } }
        public override string XPATH_SIZE { get { return "/html/body/div[1]/div/div/div/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/div/ul[2]/li[3]"; } }
        public override string XPATH_TITLE { get { return "/html/body/div[1]/div/div/div/div[2]/div[2]/div/div/div[1]/h5"; } }

        public virtual string MAGNET_XPATH { get { return "//*[@id='ping']/div/div/div[2]/div[3]/div[2]/div[2]/a"; } }

        public override string XPATH_TABLE_RESULT { get { return "/html/body/div[1]/div/div/div/div[2]/div[2]/div/div[2]/table/tbody"; } }

        public override string MARKER_CLASS_MOVIES { get { return "fa fa-video-camera"; } }
        public override string MARKER_CLASS_SERIES { get { return "fa fa-desktop"; } }

       

        public override string GetSearchUrl(string keyword)
        {
            keyword = CleanKeyword.Clean(keyword);
            keyword = keyword.Replace(" ", "-");
            return HOST + SEARCH_PATH + keyword + KEYWORD_SUFFIX ;
        }

        public override ResultItem GetTorrent(ResultItem rItm)
        {
            var proc = new CallExternalProgram();
            var r = proc.RunPyhonFile(proc.GetPackagePath("python3"), Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + @"script.py", new string[] { rItm.link });
            var pageNode = new HtmlDocument();
            pageNode.LoadHtml(r);
            var result = pageNode.DocumentNode.SelectSingleNode(MAGNET_XPATH).Attributes["href"].Value;
            rItm.Value = result;
            return rItm;
        }

    }
}
