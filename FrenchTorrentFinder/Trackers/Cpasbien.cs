﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FrenchTorrentFinder.Enums;
using HtmlAgilityPack;
using RestSharp;

namespace FrenchTorrentFinder.Trackers
{
    public class Cpasbien : ITracker
    {
        public static string HOST { get { return "https://cpasbien-fr.fr/"; } }
        public virtual string SEARCH_PATH { get { return "/recherche/"; } }

        public virtual string XPATH_SEED { get { return "/html/body/section/div/div/div/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[2]/div/ul[1]/li[3]"; } }
        public virtual string XPATH_LEECH { get { return "/html/body/section/div/div/div/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[2]/div/ul[1]/li[7]"; } }
        public virtual string XPATH_SIZE { get { return "/html/body/section/div/div/div/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[2]/div/ul[2]/li[3]"; } }
        public virtual string XPATH_TITLE { get { return "html/body/section/div/div/div/div/div[2]/div[2]/div[2]/div/div[1]/h5"; } }

        public virtual string MAGNET_XPATH{ get { return "//*[@id='infosficher']/div[3]/a[2]"; } }

        public virtual string XPATH_TABLE_RESULT { get { return "//*[@id='gauche']/table/tbody[1]"; } }

        public virtual string MARKER_CLASS_MOVIES { get { return "Films"; } }
        public virtual string MARKER_CLASS_SERIES { get { return "Séries|S├®ries"; } }
        public Cpasbien()
        {


           
        }
      

        public override ResultItem GetTorrent(ResultItem rItm)
        {
            var proc = new CallExternalProgram();
            var r = proc.RunPyhonFile(proc.GetPackagePath("python3"), Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + @"script.py", new string[] { rItm.link });
            var pageNode = new HtmlDocument();
            pageNode.LoadHtml(r);
            var result = pageNode.DocumentNode.SelectSingleNode(MAGNET_XPATH).Attributes["href"].Value;
            rItm.Value = result;
            return rItm;
        }

        public override async Task<List<ResultItem>> SearchTorrent(string keyword, Categorie categorie)
        {  
            keyword = CleanKeyword.Clean(keyword);
            Console.WriteLine($"{this.GetType().Name} {keyword} {categorie}");
            var client = new RestClient(HOST
                 + "index.php?do=search&subaction=search");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("Content-Length", "164");
            request.AddHeader("Content-Type", "multipart/form-data; boundary=--------------------------144974536113640404318360");
            request.AddHeader("Accept-Encoding", "gzip, deflate");
            request.AddHeader("Host", "cpasbien-fr.fr");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("User-Agent", "PostmanRuntime/7.15.2");
            request.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
            request.AddParameter($"multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"story\"\r\n\r\n" + keyword + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            var doc = new HtmlDocument();
            var encod = doc.DetectEncodingHtml(response.Content);
            var b = encod.GetBytes(response.Content);
            string convstr = Encoding.Default.GetString(Encoding.Convert(encod, Encoding.Default, b));
            doc.LoadHtml(convstr);
            HtmlNode htmlNode = doc.DocumentNode.SelectSingleNode(XPATH_TABLE_RESULT);
            if (htmlNode == null)
            {
                return new List<ResultItem>();
                throw new Exception("Unable to get the <table> element of result Html: " + keyword);
            }

            var trList = htmlNode.Descendants("tr");
            
            var resultItems = trList
                 .Select(tr => tr.FirstChild)
                 .Select(x => new ResultItem()
                 {
                     Title = x.FirstChild.Attributes["title"].Value,
                     link = x.FirstChild.Attributes["href"].Value,
                     Size = x.ChildNodes[2].InnerText,
                     Seeds = x.ChildNodes[4].InnerText,
                     Peers = x.ChildNodes[6].InnerText,
                     provider = "Cpasbien"
                 })
                 .ToList();
            resultItems.ForEach(x => x.Categorie = categorie.ToString());
            return resultItems;
        }
    }

}
