﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FrenchTorrentFinder.Enums;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.Extensions.Configuration;

namespace FrenchTorrentFinder.Trackers
{
    public class Torrent9 : ITracker
    {
        public static string HOST { get { return "https://www.torrent9.pl"; } }
        public virtual string SEARCH_PATH { get { return "/recherche/"; } }

        public virtual string XPATH_SEED { get { return "/html/body/section/div/div/div/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[2]/div/ul[1]/li[3]"; } }
        public virtual string XPATH_LEECH { get { return "/html/body/section/div/div/div/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[2]/div/ul[1]/li[7]"; } }
        public virtual string XPATH_SIZE { get { return "/html/body/section/div/div/div/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[2]/div/ul[2]/li[3]"; } }
        public virtual string XPATH_TITLE { get { return "html/body/section/div/div/div/div/div[2]/div[2]/div[2]/div/div[1]/h5"; } }

        public static string MAGNET_REGEX { get { return "magnet.*\" |magnet.*\' "; } }

        public virtual string XPATH_TABLE_RESULT { get { return "/html/body/section/div/div/div/div/div[2]/div[2]/div[3]/table"; } }

        public virtual string MARKER_CLASS_MOVIES { get { return "Films"; } }
        public virtual string MARKER_CLASS_SERIES { get { return "Séries|S├®ries"; } }

        public string MAGNET_XPATH { get { return "/html/body/section/div/div/div/div/div[2]/div[2]/div[2]/div/div[2]/div[2]/div[1]/div[2]/div/div[2]/a"; } }

        public virtual string GetSearchUrl(string keyword)
        {
            keyword = CleanKeyword.Clean(keyword);
            keyword = keyword.Replace("-", "%20");
            keyword = keyword.Replace(" ", "%20");

            return HOST + SEARCH_PATH + keyword; ;
        }

        public Torrent9()
        {

        }
        private string GetMarkerCategorie(Categorie categorie)
        {
            switch (categorie)
            {
                case Categorie.Movies:
                    return MARKER_CLASS_MOVIES;
                case Categorie.Tv:
                    return MARKER_CLASS_SERIES;
                default:
                    return null;
            }
        }



        public override async Task<List<ResultItem>> SearchTorrent(string keyword, Categorie categorie)
        {
            try
            {
                var searchurl = GetSearchUrl(keyword);
                Console.WriteLine($"{this.GetType().Name} {keyword} {categorie}");

                var markerCategorie = GetMarkerCategorie(categorie);
                var proc = new CallExternalProgram();
                var html = proc.RunPyhonFile(proc.GetPackagePath("python3"), Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + @"script.py", new string[] { searchurl });
                if (html == null || html == "")
                    throw new Exception("Unable to request this url: " + searchurl);
                var doc = new HtmlDocument();
                var encod = doc.DetectEncodingHtml(html);
                var b = encod.GetBytes(html);
                string convstr = Encoding.Default.GetString(Encoding.Convert(encod, Encoding.Default, b));
                doc.LoadHtml(convstr);
                HtmlNode htmlNode = doc.DocumentNode.SelectSingleNode(XPATH_TABLE_RESULT);
                if (htmlNode == null)
                {
                    return new List<ResultItem>();
                    //throw new Exception("Unable to get the <table> element of result Html: " + searchurl);
                }

                var trList = htmlNode.Descendants("tr");



                var pagelinks = trList
                    .Where(x => markerCategorie == null || Regex.Match(x.ChildNodes[1].ChildNodes[0].Attributes["class"].Value, markerCategorie).Success)
                    .Select(x => new ResultItem()
                    {
                        Title = x.ChildNodes[1].InnerText,
                        link = x.Descendants("a").First().Attributes["href"].Value,
                        Size = x.ChildNodes[3].InnerText,
                        Seeds = x.ChildNodes[5].InnerText,
                        Peers = x.ChildNodes[7].InnerText,
                        provider = this.GetType().Name
                    })
                    .ToList();
                if (pagelinks.Any() && !pagelinks.First().link.StartsWith("http"))
                {
                    pagelinks.ForEach(x => x.link = HOST + x.link);
                }
                if (pagelinks.Any() && pagelinks.First().Title.StartsWith(" "))
                {
                    pagelinks.ForEach(x => x.Title = x.Title.Remove(0, 1));
                }
                pagelinks.ForEach(x => Console.WriteLine(x));
                pagelinks.ForEach(x => x.Categorie = categorie.ToString());

                return pagelinks;
            }
            catch (Exception ex)
            {
                // throw ex;
                return new List<ResultItem>();
            }

        }

        public override ResultItem GetTorrent(ResultItem rItm)
        {
            var proc = new CallExternalProgram();
            var r = proc.RunPyhonFile(proc.GetPackagePath("python3"), Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + @"script.py", new string[] { rItm.link });
            var pageNode = new HtmlDocument();
            pageNode.LoadHtml(r);
            var result = pageNode.DocumentNode.SelectSingleNode(MAGNET_XPATH).Attributes["href"].Value;
            // var result = pageNode.DocumentNode.SelectSingleNode("/html/body/section/div/div/div/div/div[2]/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div/a").Attributes["href"].Value;
            //rItm.Value = result.Remove(result.Length - 2, 2);
            rItm.Value = result;
            return rItm;
        }
    }
}
