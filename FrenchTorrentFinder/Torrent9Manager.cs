﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrenchTorrentFinder.Enums;
using FrenchTorrentFinder.Trackers;
using Microsoft.Extensions.Configuration;

namespace FrenchTorrentFinder
{
    public class TorrentsManager
    {

        public TorrentsManager()
        {

        }


        public List<ResultItem> SearchTorrent(string keyword, int season_number, int episode_number)
        {
            TorrentContainer.Clear();
            var torrent9 = new Trackers.Torrent9();
            var torrent9Variant = new Torrent9V1();
            var yggtorrent = new Yggtorrent();
            var keywords = new List<string>();
            keywords.Add($"{keyword} S{season_number.ToString("00")} E{episode_number.ToString("00")}");
            keywords.Add($"{keyword} S{season_number.ToString("00")}");
            keywords.Add($"{keyword} saison {season_number}");


            var cpasbien = new Trackers.Cpasbien();
            //var r = cpasbien.SearchTorrent(keywords.ToArray(), Categorie.Series).Result;
            //  var r = torrent9.SearchTorrent(keword, categorie);
            var tasks = new List<Task<List<ResultItem>>>();
            tasks.Add(torrent9.SearchTorrent(keywords.ToArray(), Categorie.Tv));
            //tasks.Add(cpasbien.SearchTorrent(keywords.ToArray(), Categorie.Tv));
            tasks.Add(yggtorrent.SearchTorrent(keywords.ToArray(), Categorie.Tv));
            var totalList = new ConcurrentBag<ResultItem>();
            object listLock = new object();
            Parallel.ForEach(tasks, async a =>
            {
                if (a.Status == TaskStatus.Faulted) return;
                foreach (var itm in a.Result)
                {
                    totalList.Add(itm);
                }

                // Even more code...
            });




          
            if (totalList.Any())
                return TorrentContainer.Set(totalList.ToList());
            else
                return new List<ResultItem>();

        }
        public List<ResultItem> SearchTorrent(string keword, Categorie categorie = Categorie.All)
        {
            TorrentContainer.Clear();
            var torrent9 = new Trackers.Torrent9();
            var yygTorrent = new Yggtorrent();
            var cpasbien = new Trackers.Cpasbien();
            var torrent9Variant = new Torrent9V1();
     

            var tasks = new List<Task<List<ResultItem>>>();
            tasks.Add(torrent9.SearchTorrent(keword, categorie));
            //tasks.Add(cpasbien.SearchTorrent(keword, categorie));
            tasks.Add(yygTorrent.SearchTorrent(keword, categorie));

            var totalList = new ConcurrentBag<ResultItem>();
            object listLock = new object();
            Parallel.ForEach(tasks, async a =>
            {
                if (a.Status == TaskStatus.Faulted) return;
                foreach (var itm in a.Result)
                {
                    totalList.Add(itm);
                }

                // Even more code...
            });
            if (totalList.Any())
                return TorrentContainer.Set(totalList.ToList());
            else
                return new List<ResultItem>();
        }

    }
}
