﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FrenchTorrentFinder.Enums;
using FrenchTorrentFinder.Trackers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;

namespace FrenchTorrentFinder.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TorrentController : ControllerBase
    {

        private readonly IHostingEnvironment _hostingEnvironment;
        IConfiguration _configuration;
        private IMemoryCache _cache;
        public TorrentController(IHostingEnvironment hostingEnvironment, IConfiguration configuration, IMemoryCache memoryCache)
        {
            _cache = memoryCache;
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
            torrent9Manager = new TorrentsManager();
        }
        private readonly TorrentsManager torrent9Manager;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(TorrentController));

        // GET api/values
        [HttpGet("Search/{categorie}/{keyword?}")]
        public ActionResult<string> SearchWithCategory(string categorie, string keyword, bool json = false, bool useCache = true)
        {
            try
            {
                Categorie searchCategorie = Categorie.All;
                if (!string.IsNullOrEmpty(categorie))
                    searchCategorie = (Categorie)Enum.Parse(typeof(Categorie), categorie);
                var result = Find(searchCategorie, keyword, 0, 0, json, useCache);
                if (json == true)
                    return new JsonResult(result);
                var formater = new FormatResult();
                return formater.Format(result);
            }
            catch (Exception ex)
            {
                log.Info(ex.ToString());
                throw;
            }

        }

        // GET api/values
        [HttpGet("search/{categorie}/{keyword}/season/{season_number?}/episode/{episode_number?}")]
        public ActionResult<string> SearchSeries(string keyword, int season_number, int episode_number, bool json = false, bool useCache = true)
        {
            try
            {

                var result = Find(Categorie.Tv, keyword, season_number, episode_number, json, useCache);
                if (json == true)
                    return new JsonResult(result);
                var formater = new FormatResult();
                return formater.Format(result);
            }
            catch (Exception ex)
            {
                log.Info(ex.ToString());
                throw;
            }

        }
        // GET api/values
        [HttpGet("Search/{keyword?}")]
        public ActionResult<string> Search(string keyword, bool json = false, bool useCache = true)
        {
            try
            {
                var result = Find(Categorie.All, keyword, 0, 0, json, useCache);
                if (json == true)
                    return new JsonResult(result);
                var formater = new FormatResult();
                return formater.Format(result);
            }
            catch (Exception ex)
            {
                log.Info(ex.ToString());
                throw;
            }

        }


        [HttpGet("Download/{filename}")]
        public async Task<IActionResult> Download(string filename)
        {
            var filepath = Path.Combine(Directory.GetCurrentDirectory(), "Torrents", filename);
            byte[] readStream =  System.IO.File.ReadAllBytes(filepath);
            var mimeType = "application/octet-stream";
            return File(readStream, mimeType);
        }

        private List<ResultItem> Find(Categorie categorie, string keyword, int season_number, int episode_number, bool json = false, bool useCache = true)
        {
            var ap = categorie + keyword + season_number + episode_number;
            List<ResultItem> cacheEntry = new List<ResultItem>();

            // Look for cache key.
            if (!useCache || !_cache.TryGetValue(ap, out cacheEntry))
            {
                log.Info("Force request");

                if (categorie == Categorie.Tv)
                {
                    cacheEntry = torrent9Manager.SearchTorrent(keyword, season_number, episode_number);
                }
                else
                {
                    cacheEntry = torrent9Manager.SearchTorrent(keyword, categorie);
                }

            }
            log.Info(_hostingEnvironment.ContentRootPath + @"\script.py");
            // Set cache options.
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                // Keep in cache for this time, reset time if accessed.
                .SetSlidingExpiration(TimeSpan.FromSeconds(7200));
            // Save data in cache.
            if (cacheEntry.Any())
                _cache.Set(ap, cacheEntry, cacheEntryOptions);
            return cacheEntry;
        }


        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id, bool json = false)
        {
            Console.WriteLine(Path.Combine(Directory.GetCurrentDirectory(), "Torrents"));
            var result = TorrentContainer.GetResultItem(id);
            if (json == true)
                return new JsonResult(result);
            var formater = new FormatResult();
            return formater.Format(result);
        }


    }
}
