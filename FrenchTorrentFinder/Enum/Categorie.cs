﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrenchTorrentFinder.Enums
{
    public enum Categorie
    {
        Movies = 1,
        Tv = 2,
        All = 3
    }
}
