﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FrenchTorrentFinder
{
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class ResultItem {
        public string Title { get; set; }
        public DateTime? time { get; set; }
        public string Seeds { get; set; }
        public string Peers { get; set; }
        public string Size { get; set; }
        public string desc { get; set; }
        public string id { get; set; }
        public string provider { get; set; }
        public string link { get; set; }
        public string Value { get; internal set; }
        public string Categorie { get; internal set; }

        public override string ToString()
        {
            return $"{this.Title} {this.Size} {this.Seeds} {this.Peers} {this.link}"; 
        }
    }
}
