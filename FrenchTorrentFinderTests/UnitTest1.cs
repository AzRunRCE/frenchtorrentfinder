using NUnit.Framework;
using FrenchTorrentFinder;
using System;
using System.IO;
using FrenchTorrentFinder.Trackers;
using System.Threading.Tasks;

namespace Tests
{
    public class Tests
    {

        private string PathContent;
        [SetUp]
        public void Setup()
        {
            Directory.SetCurrentDirectory(AppContext.BaseDirectory.Replace("FrenchTorrentFinderTests", "FrenchTorrentFinder"));

        }

        [Test]
        public void TestTorrentManager()
        {
            TorrentsManager t9Mgr = new TorrentsManager();
            var result = t9Mgr.SearchTorrent("stranger things s03e08");
            Assert.AreNotEqual(0, result.Count);
        }

        [Test]
        public async Task TestTorrentWithFilter()
        {
            Torrent9V1 t9Mgr = new Torrent9V1();
            var result = await t9Mgr.SearchTorrent("stranger things s03e08", FrenchTorrentFinder.Enums.Categorie.Movies);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public async Task TestTorrent9V1()
        {
            Torrent9V1 t9Mgr = new Torrent9V1();
            var result = await t9Mgr.SearchTorrent("stranger things s03e08", FrenchTorrentFinder.Enums.Categorie.All);
            Assert.AreNotEqual(0, result.Count);
        }

        [Test]
        public async Task TestTorrent9()
        {

            Torrent9 t9Mgr = new Torrent9();
            var result = await t9Mgr.SearchTorrent("stranger things s03e08", FrenchTorrentFinder.Enums.Categorie.Tv);
            Assert.AreNotEqual(0, result.Count);
        }

        [Test]
        public async Task GetTorrentCpasbien()
        {

            Cpasbien t9Mgr = new Cpasbien();
            var result = t9Mgr.GetTorrent(new ResultItem() { link = "https://cpasbien-fr.fr/torrent/33545/Falling-Inn-Love-FRENCH-WEBRIP-720p-2019-telecharger" });
            Assert.NotNull(result);
        }

        [Test]
        public async Task yggtorrentTest()
        {

            Yggtorrent t9Mgr = new Yggtorrent();
            var result = t9Mgr.SearchTorrent("westworld", FrenchTorrentFinder.Enums.Categorie.Tv);
            Assert.AreNotEqual(0, result.Result.Count);
        }
    }
}